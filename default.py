import butler_interface
import logging
import json
import requests
from storage_strategy import get_storage_strategy

logging.basicConfig(filename="update_storage_strategy.log",
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.DEBUG, filemode='w')
log = logging.getLogger(__name__)
with open('config.json') as f:
    data = json.load(f)
    PRODUCT_STORAGE_STRATEGY_KEY = data.get('PRODUCT_STORAGE_STRATEGY_KEY', 'vertical')
    BASE_API_URL = data['BASE_API_URL']
    PRODUCT_URL = '%s/products/' % BASE_API_URL
    username=data['username']
    password=data['password']
    data={
          "username": username,
          "password": password
        }
    AUTH_URL = '%s/auth/token/' % BASE_API_URL
    response=requests.post(AUTH_URL,json=data, verify=False)
    response = json.loads(response.content)
    token=response.get('auth_token')
    header = {'Accept': 'application/json', 'Content-Type': 'application/json','Authentication-Token':str(token)}


def fetch_product(product_uid):
    url = PRODUCT_URL+product_uid
    res = requests.get(url, headers=header, verify=False)
    if res.status_code == 200:
        json_data = json.loads(res.text)
        return json_data['product']
    else:
        log.error("Error while product URL: %s" % url)

if __name__ == "__main__":
    f = open('/tmp/test.csv', "r")
    product_uids = f.readlines()
    log.debug("File read succesfully %s", len(product_uids))
    for uid in product_uids:
        uid = uid.rstrip("\n")
        product = fetch_product(uid)
        pdfa_values = product['pdfa_values']
        extra_fields = product['extra_fields']
        dimensions = extra_fields['product_dimensions']
        extra_fields[PRODUCT_STORAGE_STRATEGY_KEY] = get_storage_strategy(dimensions)
        butler_interface.update_product(pdfa_values, extra_fields)

