from collections import namedtuple

# depth, length, height
Dimension = namedtuple('Dimension', ['length', 'depth', 'height'])
LARGE_SLOT = Dimension(length=90, depth=63, height=55)  # anything bigger than this will go to the large slot
MEDIUM_SLOT = Dimension(length=60, depth=48, height=35)  # anything smaller than this will go to the medium slot
SMALL_SLOT = Dimension(length=28, depth=63, height=15)  # anything smaller than this will go to the small slot


def get_storage_strategy(dimensions):
    # first check if the item can fit in small slot
    if (item_fit(dimensions, SMALL_SLOT) == True):
        return "small_size_storage"
    # else if it can it can fit in medium slot
    elif (item_fit(dimensions, MEDIUM_SLOT) == True):
        return "medium_size_storage"
    # else just return large
    else:
        return "large_size_storage"


def item_fit(product_dimensions, slot):
    dimension_sum = product_dimensions[0] + product_dimensions[1] + product_dimensions[2]
    is_fitting = False

    for i, dim1 in enumerate(product_dimensions):
        for j, dim2 in enumerate(product_dimensions):
            if i != j:
                dim3 = dimension_sum - dim1 - dim2
                if check_dimension_fit(dim1, dim2, dim3, slot):
                    is_fitting = True

    return is_fitting


# Filters based on dimensions.
# In case any two dimension are smaller length and height of the bin then the product can fir
# Then see if the 3 dimension is smaller than the depth of the bin
def check_dimension_fit(dim1, dim2, dim3, slot):
    if (dim1 <= slot.length and dim2 <= slot.height and dim3 <= slot.depth):
        if dim2/dim1 < 3 and dim2/dim3 < 3:
            return True

    return False