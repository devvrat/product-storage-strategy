import requests
import logging
import json

with open('config.json') as f:
    data = json.load(f)
    BASE_API_URL = data['BASE_API_URL']
    PRODUCT_STORAGE_STRATEGY_KEY = data.get('PRODUCT_STORAGE_STRATEGY_KEY', 'vertical')
    username=data['username']
    password=data['password']
    data={
          "username": username,
          "password": password
        }
    AUTH_URL = '%s/auth/token/' % BASE_API_URL
    response=requests.post(AUTH_URL,json=data, verify=False)
    response = json.loads(response.content)
    token=response.get('auth_token')
    header = {'Accept': 'application/json', 'Content-Type': 'application/json','Authentication-Token':str(token)}
    print header

PRODUCTS_URL = '%s/products/all/' % BASE_API_URL
PRODUCTS_PAGED_URL = '%s/products/' % BASE_API_URL
UPDATE_PRODUCTS_URL = '%s/products?sync=true' % BASE_API_URL
CREATE_PDEA_URL = '%s/exhaustive_pdea/' % BASE_API_URL

log = logging.getLogger(__name__)


def delete_system_extra_fields(extra_fields):
    del extra_fields['handling_type']
    return extra_fields


def update_product(pdfa_values, extra_fields):
    extra_fields = delete_system_extra_fields(extra_fields)
    data = {
        'product_type': [{
            'pdfa_values': pdfa_values,
            'extra_fields': extra_fields
        }]
    }

    res = requests.put(UPDATE_PRODUCTS_URL, json=data, headers=header, verify=False)
    if res.status_code == 200:
        log.info("Successfully updated product for SKU %s and dimension %s with storage strategy: %s" % (
            pdfa_values['product_sku'],
            extra_fields['product_dimensions'], extra_fields[PRODUCT_STORAGE_STRATEGY_KEY]))
    else:
        log.error("Error while updating SKU %s: %s" % (pdfa_values['product_sku'], res.status_code))


def fetch_all_products():
    res = requests.get(PRODUCTS_URL, headers=header,verify=False)
    if res.status_code == 200:
        log.info("Successfully fetched product list")
        return (json.loads(res.text))['products']
    else:
        log.error("Error while fetching the product list")


def fetch_products(url=None):
    if url is None:
        url = PRODUCTS_PAGED_URL

    res = requests.get(url, headers=header, verify=False)
    if res.status_code == 200:
        json_data = json.loads(res.text)
        return (json_data['next_page'], json_data['products'])
    else:
        log.error("Error while product URL: %s" % url)


def create_vertical_pdea():
    data = {
        "exhaustive_pdea": [
            {
                "field_name": "vertical",
                "field_data_type": "string"
            }
        ]
    }

    res = requests.post(CREATE_PDEA_URL, json=data, headers=header, verify=False)
    if res.status_code == 200:
        log.info("Created PDEA: vertical")
        log.info(res.text)
        return res.status_code
    elif res.status_code == 400:
        ## Check if vertical was already created. That is also good for us
        try:
            unsuccessful = (json.loads(res.text))['unsuccessful']
            if unsuccessful[0]['alert_data'][0]['code'] == "e125":
                log.info("Vertical PDEA was already created")
                return 200
        except:
            log.error(res.text)
            log.warn("Couldn't create PDEA")
            return res.status_code
    else:
        log.error(res.text)
        log.warn("Couldn't create PDEA")
        return res.status_code
