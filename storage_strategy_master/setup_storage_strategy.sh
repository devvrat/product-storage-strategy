echo "*************************************"
echo "Starting to curl storage strategy and storage tags for size distribution"
echo "*************************************"
echo ""
echo "Enter hostname (e.g. http://127.0.0.1:5000) of the Interface Server> "
read ip

echo ">>>>>>>>>>>>>>>>> Fetching auth token..."
token=$(curl -i -X POST -k  -H 'Content-Type: application/json' -H 'Accept: application/json' -d@userpass.json $ip/api/auth/token | grep -Po '(?<="auth_token": ")[^"]*')

echo "\n\n<<<<<<<<<<<<< Fetched auth token:"
echo $token

echo "\n\n>>>>>>>>>>>>> Curling storage tags"
curl -i -X POST -k -H 'Content-Type: application/json' -H 'Accept: application/json' -H "Authentication-Token:$token" -d@storage_tags.json $ip/api/storage_tags

echo "\n\n>>>>>>>>>>>>> Curling small size storage"
curl -i -X POST -k -H 'Content-Type: application/json' -H 'Accept: application/json' -H "Authentication-Token:$token" -d@small_size_storage.json $ip/api/storage_strategy

echo "\n\n>>>>>>>>>>>>> Curling medium size storage"
curl -i -X POST -k -H 'Content-Type: application/json' -H 'Accept: application/json' -H "Authentication-Token:$token" -d@medium_size_storage.json $ip/api/storage_strategy

echo "\n\n>>>>>>>>>>>> Curling large size storage"
curl -i -X POST -k -H 'Content-Type: application/json' -H 'Accept: application/json' -H "Authentication-Token:$token" -d@large_size_storage.json $ip/api/storage_strategy

echo "\n\n>>>>>>>>>>>> Curling default storage"
curl -i -X POST -k -H 'Content-Type: application/json' -H 'Accept: application/json' -H "Authentication-Token:$token" -d@default_storage.json $ip/api/storage_strategy

echo "\nDone"
