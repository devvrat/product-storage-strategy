import butler_interface
import logging
import json
from storage_strategy import get_storage_strategy

logging.basicConfig(filename="update_storage_strategy.log",
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.DEBUG, filemode='w')

log = logging.getLogger(__name__)

with open('config.json') as f:
    data = json.load(f)
    PRODUCT_STORAGE_STRATEGY_KEY = data.get('PRODUCT_STORAGE_STRATEGY_KEY', 'vertical')

if __name__ == "__main__":
    if butler_interface.create_vertical_pdea() == 200:
        next_product_url = None
        count = 0
        while True:
            (next_product_url, products) = butler_interface.fetch_products(next_product_url)
            if products is None:
                log.debug("None product found for url %s", next_product_url)
            else:
                for product in products:
                    pdfa_values = product['pdfa_values']
                    extra_fields = product['extra_fields']
                    dimensions = extra_fields['product_dimensions']
                    strategy = get_storage_strategy(dimensions)
                    if strategy == extra_fields.get(PRODUCT_STORAGE_STRATEGY_KEY, ''):
                        continue
                    else:   
                        extra_fields[PRODUCT_STORAGE_STRATEGY_KEY] = get_storage_strategy(dimensions)
                        butler_interface.update_product(pdfa_values, extra_fields)

            count = count + 1
            log.debug("Count of URL fetch: %s" % count)
            log.debug("Next product URL is %s", next_product_url)
            if next_product_url == "null":
                break
    else:
        log.error("Couldn't create PDEA exiting now")
